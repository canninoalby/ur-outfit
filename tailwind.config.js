/** @type {import('tailwindcss').Config} */
const plugin = require('tailwindcss/plugin')
module.exports = {
  content: ["./women.html", "./index.html", "./dos.html", "./project.html", "./*.html"],
  darkMode: "class", // or 'media' or 'class'
  theme: {
    container: {
      center: true,
    },
    extend: {
      colors: {
        primary: "#D9D9D9E8",
        secondary: "#33444B",
        neutral: "#f3f4f6",
        vercel: "#0284c7",
        netlify: "#2563eb",
        github: "#57534e",
        coffee: "#d97706",
        kofi: "#db2777",
        dark: "#262626",
      },
      textShadow: {
        sm: '0 1px 2px var(--tw-shadow-color)',
        DEFAULT: '0 2px 4px var(--tw-shadow-color)',
        lg: '0 8px 16px var(--tw-shadow-color)',
      },
      screens: {
        xl: "1320px",
      },
    },
  },
  plugins: [
    plugin(function ({ matchUtilities, theme }) {
      matchUtilities(
        {
          'text-shadow': (value) => ({
            textShadow: value,
          }),
        },
        { values: theme('textShadow') }
      )
    }),
    require('flowbite/plugin'),
  ],
};
