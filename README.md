![My Image](image.png)

## Run Locally

**Start the Tailwind CLI build process**

Run the CLI tool to scan your template files for classes and build your CSS.

```bash
  npx tailwindcss -i ./src/input.css -o ./dist/output.css --watch
```

## 📚 References

[**TailwindCSS**](https://tailwindcss.com/)
